<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/', function () {
    //return view('welcome'); //call resources>views>welcome.blade.php
    //return 'Hello mimiko'; 
    //return view('welcome')->with('gretting','mimiko');
});*/

Route::get('/','PostController@index');

//Blog Post
Route::get('post/{post}','PostController@show');

/*Upload post*/
Route::get('/upload','PostController@create');

Route::post('/upload','PostController@store');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
//HC>index function

Route::get('/category','CategoryController@index');
Route::get('/categoryinput','CategoryController@create');
Route::post('/categoryinput','CategoryController@store');


Route::post('/comment','CommentController@store');

Route::get('/post/delete/{post}','PostController@destroy');
Route::get('/post/edit/{post}','PostController@edit');
Route::post('/post/edit','PostController@update');


Route::get('/category/delete/{post}','CategoryController@destroy');
Route::get('/category/edit/{post}','CategoryController@edit');
Route::post('/category/edit','CategoryController@update');



Route::get('admin-login','Auth\AdminLoginController@showLoginForm');  //to show login form
Route::post('admin-login','Auth\AdminLoginController@login');
Route::get('/backend','AdminController@index');//to go backend













