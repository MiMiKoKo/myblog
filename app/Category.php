<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Post;

class Category extends Model
{
    //
    protected $fillable=['id','category_name']; 

    public function user()
    {
    	return $this->belongsTo(User::class);
    }  

    public function posts()
    {
    	return $this->hasMany(Post::class);
    }

}
