<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post;
use App\Category;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware('auth',['except'=>['index','show']]);
    }
    public function index()
    {
        //

        /*$posts=Post::all();*/

        $postQuery=Post::query();
        $postQuery->latest();

        if($cat_id=request('cat_id')){
            $postQuery->where('category_id',$cat_id);
        }

        $posts =$postQuery->get();
        
        $categories=Category::all();
        return view('blog',compact('posts','categories'));//to send data to blog
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $categories=Category::all();
        return view('post.create',compact('categories'));

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        //dd($request); //to view input data
        $this->validate(request(),[

            'title' => 'required|min:5',
            'photo' => 'required|image|mimes:jpeg,png,ipg,gif,svg|max:2048',
            'body' => 'required|min:10'
        ]);


        $imageName=time().'.'.request()->photo->getClientOriginalExtension();
        request()->photo->move(public_path("images"),$imageName);

        $fullImg='/images/'.$imageName;

        /*to insert data to table col-name => request(name of input)*/
        Post::create([
            'title'=>request('title'),
            'category_id'=>request('categorylist'),
            'photo'=>$fullImg,
            'body'=>request('body'),
            'user_id'=>auth()->id() //login user id
        ]);


        return redirect("/");//go to localhost/
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Post $post) //Post=model ,$post=web.php>Route::get('post/{post}','PostController@show');
    {
        //
        //dd($post);
       /* return view('blogpost');*/
        return view('blogpost',compact('post'));//compact=send data
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Post $post)
    {
        //
        $categories=Category::all();
        //dd($categories);
        return view('post.edit',compact('post','categories'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        //
         //
        //dd($request); //to view input data
        $this->validate(request(),[

            'title' => 'required|min:5',
            'photo' => 'required|image|mimes:jpeg,png,ipg,gif,svg|max:2048',
            'body' => 'required|min:10'
        ]);


        $imageName=time().'.'.request()->photo->getClientOriginalExtension();
        request()->photo->move(public_path("images"),$imageName);

        $fullImg='/images/'.$imageName;

        $id=request('editid');
        $post=Post::find($id);
        $post->title=request('title');
        $post->category_id=request('categorylist');
        $post->photo=$fullImg;
        $post->body=request('body');
        $post->user_id=auth()->id();
        $post->save();

        return redirect('/');//go to localhost/
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Post $post)
    {
        //
            foreach ($post->comments as $comment):
                $comment->delete();
            endforeach;

            $post->delete();
            return redirect('/')->with('msg','your post deleted');
    }
}
