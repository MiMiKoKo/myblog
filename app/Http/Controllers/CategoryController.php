<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;
use App\POST;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('is_admin');
    }
    public function index()
    {
        //
        $categories=Category::all();
        return view('category',compact('categories'));//to send data to blog
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('category.create');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        /*$this->validate(request(),[
            'name' => 'required|min:5'            
        ]);       
*/
        Category::create([
            'category_name'=>request('name')           
        ]);

        return redirect("/category");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Category $post)
    {
        //
        //
        $categories=Category::all();
        /*dd($categories);*/
        return view('category.edit',compact('post','categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $this->validate(request(),[
            'name' => 'required|min:3'           
        ]);
        
        $id=request('id');
        $post=Category::find($id);
        $post->category_name=request('name');        
        $post->save();

        return redirect('/category');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Category $post)
    {
        //
            foreach ($post->posts as $id):
                $id->delete();
            endforeach;

            $post->delete();
            return redirect('/category')->with('msg','your category deleted');
    }
}
