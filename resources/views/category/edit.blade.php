@extends('layouts.template')

@section('content')

<div class="col-md-8 mt-5">

	   @if(count($errors))
	    <div class="alert alert-danger">
	    	<ul>
	    		@foreach($errors->all() as $error)
	    		  <li><p>{{ $error }}</p></li>
	            @endforeach
	    	</ul>	    	
	    </div>
	   @endif

	<form action="/category/edit" method="post" class="my-3">
		@csrf
		<input type="hidden" name="id" value="{{ $post->id }}">

		<div class="form-group">
			<label>Category Input Form</label>
	    </div>

	    <div class="form-group">
			<label>Category Name</label>
			<input type="text" name="name" value="{{$post->category_name}}" class="form-control">			
	    </div>
		
		<div class="form-group">
			<input type="submit" name="submit" class="btn btn-primary" value="Update">
		</div>
	</form>
	
</div>
@endsection