@extends('layouts.template')

@section('content')
	 <div class="col-md-8">

          <h1 class="my-4"> 
            <small>Category Information Table</small>
          </h1>    
     

         <a href="/categoryinput" class="btn btn-outline-secondary mb-2">Add Category</a>

         <div class="container-fluid">
            <table class="table table-bordered">
              <thead>
                <tr>
                  <th scope="col">No</th>
                  <th scope="col">Name</th>      
                  <th scope="col">Option</th>      
                </tr>
              </thead>
                <tbody>                      
                  @foreach($categories as $category)
                      <tr>
                        <td>{{ $category->id}}</td>
                        <td>{{ $category->category_name}}</td> 
                        <td>
                          <a href="/category/edit/{{$category->id}}" type="button" class="btn btn-primary ml-3">Edit</a>
                          <a href="/category/delete/{{$category->id}}" type="button" class="btn btn-danger ml-3">Delete</a></td>            
                      </tr>                        
                  @endforeach

                </tbody>
              </table>
          </div> 

          <ul class="pagination justify-content-center mb-4">
            <li class="page-item">
              <a class="page-link" href="#">&larr; Older</a>
            </li>
            <li class="page-item disabled">
              <a class="page-link" href="#">Newer &rarr;</a>
            </li>
          </ul>

        </div>

@endsection