@extends('layouts.template')

@section('content')

<div class="col-md-8 mt-5">	   


	<!-- to view validation errors -->
	   @if(count($errors))
	    <div class="alert alert-danger">
	    	<ul>
	    		@foreach($errors->all() as $error)
	    		  <li><p>{{ $error }}</p></li>
	            @endforeach
	    	</ul>	    	
	    </div>
	   @endif

	<form action="/post/edit" method="post" enctype="multipart/form-data" class="my-3">
		@csrf
		
		<input type="hidden" name="editid" value="{{ $post->id }}">

		<div class="form-group">
			<label>Post Title</label>
			<input type="text" name="title" class="form-control" value="{{$post->title}}">
	    </div>

	    <div class="form-group">	    
			    <option>Choose Category name</option>
		    <select name='categorylist'>			    
			    @foreach($categories as $category)
			    <option value="{{ $category->id }}" @if($category->id==$post->category_id){{ 'selected' }}@endif>{{$category->category_name}}</option>
			    @endforeach
			</select>
	   </div>		

	    <div class="form-group">
			<label>Post Photo</label>
			<input type="file" name="photo" class="form-control-file">
			<img src="{{$post->photo}}" width="150px" height="200px">
	    </div>

		<div class="form-group">
			<label>Post Body</label>
			<textarea id="summernote" class="form-control" name="body">{!! $post->body !!}</textarea>	
		</div>

		<div class="form-group">
			<input type="submit" name="submit"  value="Update" class="btn btn-primary">
		</div>

	</form>
</div>
@endsection