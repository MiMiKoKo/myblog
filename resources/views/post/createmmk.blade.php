@extends('layouts.template')

@section('content')

<div class="col-md-8 mt-5">	   


	<!-- to view validation errors -->
	   @if(count($errors))
	    <div class="alert alert-danger">
	    	<ul>
	    		@foreach($errors->all() as $error)
	    		  <li><p>{{ $error }}</p></li>
	            @endforeach
	    	</ul>	    	
	    </div>
	   @endif

	<form action="/upload" method="post" enctype="multipart/form-data" class="my-3">
		@csrf
		<div class="form-group">
			<label>Post Title</label>
			<input type="text" name="title" class="form-control">
			
<!-- @if ($errors->has('title'))
        <span class="invalid-feedback" role="alert">
            <strong>{{ $errors->first('tile') }}</strong>
        </span>
    @endif -->

	    </div>

	     <div class="form-group">	    
			    <option>Choose Category name</option>
		    <select name='categorylist'>			    
			    @foreach($categories as $category)
			    <option value="{{ $category->id }}">{{$category->category_name}}</option>
			    @endforeach
			</select>
	   </div>

	    <div class="form-group">
			<label>Post Photo</label>
			<input type="file" name="photo" class="form-control-file">
	    </div>

		<div class="form-group">
			<label>Post Body</label>
			<textarea id="summernote" class="form-control" name="body"></textarea>	
		</div>

		<!-- <div class="form-group">
			<label>Category Name</label>
			<input type="text" name="title" class="form-control">				
		</div> -->

		<div class="form-group">
			<input type="submit" name="submit" class="btn btn-primary">
		</div>

	</form>
</div>
@endsection