@extends ('layouts.template')

@section('content')
  <div class="col-md-8">

          <!-- Title -->
          <h1 class="mt-4">{{ $post-> title }}</h1>

          <!-- Author -->
          <p class="lead">
            by
            <a href="#">{{ $post-> user->name }}</a>

            @if(Auth::check() && ($post->user_id==auth()->id()))
              <a href="/post/edit/{{$post->id}}">Edit</a>
              <a href="/post/delete/{{$post->id}}">Delete</a>
            @endif

          </p>

          <hr>

          <!-- Date/Time -->
          <p>Posted on {{ $post->created_at->toDayDateTimeString() }}</p>

          <hr>

          <!-- Preview Image -->
          <img class="img-fluid rounded" src="{{ $post->photo}}" alt="">

          <hr>

          {{ $post-> body }}          
          <!-- {!! $post-> body !!}   -->        

          <hr>

          @if(!Auth::check())
            <div class="alert alert-danger">
              You must login first!
            </div>
          @endif

          <!-- Comments Form -->
          <div class="card my-4">
            <h5 class="card-header">Leave a Comment:</h5>
            <div class="card-body">
              <form method="post" action="/comment">
                @csrf
                <input type="hidden" name="postid" value="{{ $post->id }}">
                <div class="form-group">
                  <textarea name="comment" class="form-control" rows="3"></textarea>
                </div>
                <button type="submit" class="btn btn-primary">Submit</button>
              </form>
            </div>
          </div> 

          

                   <!-- Single Comment -->
        @foreach($post->comments as $comment)
        <div class="media mb-4">
          <img class="d-flex mr-3 rounded-circle" width="50px" height="50px" alt="">
          <div class="media-body">
            <h5 class="mt-0">{{$comment->user->name}}</h5>
            {{$comment->body}}<br>
            <i style="color: blue">{{$comment->created_at->diffForHumans()}}</i>
          </div>
        </div>
        @endforeach   

  </div>
@endsection